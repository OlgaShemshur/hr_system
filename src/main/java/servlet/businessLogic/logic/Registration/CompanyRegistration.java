package servlet.businessLogic.logic.Registration;

import servlet.businessLogic.entities.Company;
import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.User;
import servlet.businessLogic.logic.DataGet;
import servlet.dao.DAOCompany;

import java.sql.SQLException;
import java.util.ArrayList;

public class CompanyRegistration implements RegistrationInterface {
    private Company company;

    public CompanyRegistration(String name, String address, User user){
        company = new Company();
        company.setDefault();
        company.setIduser(user.getId());
        company.setName(name);
        company.setAddress(address);
    }


    @Override
    public boolean checkMatchInDB() throws SQLException, InterruptedException {
        DAOCompany data = new DAOCompany();
        String[] field = {"name"};
        String[] value = {company.getName()};
        ArrayList<Entity> arrayList = data.getEntityByField(field, value);
        if (arrayList.size() == 1) return true;
        else return false;
    }

    @Override
    public boolean register() throws SQLException, InterruptedException {
        if (!checkMatchInDB()) {
            if (!company.getName().isEmpty() && company.getIduser() != 0) {
                DataGet.createEntity(company);
                return true;
            } else return false;
        } else return false;
    }
}
