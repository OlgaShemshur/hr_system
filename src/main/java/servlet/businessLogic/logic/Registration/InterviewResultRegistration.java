package servlet.businessLogic.logic.Registration;

import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.Interview;
import servlet.businessLogic.entities.InterviewResult;
import servlet.businessLogic.entities.User;
import servlet.businessLogic.logic.DataGet;
import servlet.dao.DAOInterview;
import servlet.dao.DAOInterviewResult;

import java.sql.SQLException;
import java.util.ArrayList;

public class InterviewResultRegistration implements RegistrationInterface {
    private InterviewResult interviewResult;

    public InterviewResultRegistration(Integer IDinterview, Double mark, String recall){
        interviewResult = new InterviewResult();
        interviewResult.setDefault();
        interviewResult.setInterviewid(IDinterview);
        interviewResult.setAverageMark(mark);
        interviewResult.setRecall(recall);
    }

    @Override
    public boolean checkMatchInDB() throws SQLException, InterruptedException {
        DAOInterviewResult data = new DAOInterviewResult();
        String[] field = {"idinterview"};
        String[] value = {String.valueOf(interviewResult.getInterviewid())};
        ArrayList<Entity> arrayList = data.getEntityByField(field, value);
        if (arrayList.size() == 1) return true;
        else return false;
    }

    @Override
    public boolean register() throws SQLException, InterruptedException {
        if (!checkMatchInDB()) {
            if (!String.valueOf(interviewResult.getInterviewid()).isEmpty()) {
                DataGet.createEntity(interviewResult);
                return true;
            } else return false;
        } else return false;
    }
}
