package servlet.businessLogic.logic.Registration;

import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.Interview;
import servlet.businessLogic.entities.User;
import servlet.businessLogic.entities.Vacancy;
import servlet.businessLogic.logic.DataGet;
import servlet.dao.DAOCompany;
import servlet.dao.DAOInterview;

import java.sql.SQLException;
import java.util.ArrayList;

public class InterviewRegistration implements RegistrationInterface {
    private Interview interview;

    public InterviewRegistration(Integer IDvacancy, User user){
        interview = new Interview();
        interview.setDefault();
        interview.setIdapplicant(user.getId());
        interview.setIdvacancy(IDvacancy);
    }

    @Override
    public boolean checkMatchInDB() throws SQLException, InterruptedException {
        DAOInterview data = new DAOInterview();
        String[] field = {"idapplicant", "idvacancy"};
        String[] value = {String.valueOf(interview.getIdapplicant()), String.valueOf(interview.getIdvacancy())};
        ArrayList<Entity> arrayList = data.getEntityByField(field, value);
        if (arrayList.size() == 1) return true;
        else return false;
    }

    @Override
    public boolean register() throws SQLException, InterruptedException {
        if (!checkMatchInDB()) {
            if (!String.valueOf(interview.getIdapplicant()).isEmpty() && !String.valueOf(interview.getIdvacancy()).isEmpty()) {
                DataGet.createEntity(interview);
                return true;
            } else return false;
        } else return false;
    }
}
