package servlet.businessLogic.logic.Registration;

import servlet.businessLogic.entities.Company;
import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.User;
import servlet.businessLogic.entities.Vacancy;
import servlet.businessLogic.logic.DataGet;
import servlet.dao.DAOCompany;

import java.sql.SQLException;
import java.util.ArrayList;

public class VacancyRegistration implements RegistrationInterface {
    private Vacancy vacancy;

    public VacancyRegistration(String position, String salary, String currency, String requirements, User user){
        vacancy = new Vacancy();
        vacancy.setDefault();
        vacancy.setIdemployee(user.getId());
        vacancy.setPosition(position);
        vacancy.setRequirements(requirements);
        if (vacancy.getSalary() != 0) vacancy.setSalary(Double.parseDouble(salary));
        vacancy.setSalaryCurrency(currency);
    }


    @Override
    public boolean checkMatchInDB() throws SQLException, InterruptedException {
        DAOCompany data = new DAOCompany();
        String[] field = {"position", ""};
        String[] value = {vacancy.getPosition()};
        ArrayList<Entity> arrayList = data.getEntityByField(field, value);
        if (arrayList.size() == 1) return true;
        else return false;
    }

    @Override
    public boolean register() throws SQLException, InterruptedException {
        if (!vacancy.getPosition().isEmpty() && vacancy.getIdEmploee()!=0) {
            DataGet.createEntity(vacancy);
            return true;
        } else return false;
    }
}
