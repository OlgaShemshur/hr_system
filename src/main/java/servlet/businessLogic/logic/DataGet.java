package servlet.businessLogic.logic;

import servlet.businessLogic.entities.*;
import servlet.dao.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Olga
 */
public class DataGet {
    public static ArrayList<Entity> getALL(Entity obj) throws SQLException, InterruptedException {
        if (obj instanceof Employer) return new DAOEmployer().getAll();
        else if (obj instanceof Employee) return new DAOEmploee().getAll();
        else if (obj instanceof User) return new DAOUser().getAll();
        else if (obj instanceof Company) return new DAOCompany().getAll();
        else if (obj instanceof Interview) return new DAOInterview().getAll();
        else if (obj instanceof Vacancy) return new DAOVacancy().getAll();
        else if (obj instanceof InterviewResult) return new DAOInterviewResult().getAll();
        return null;
    }
    
    public static ArrayList<Entity> getALLByField(Entity obj, String[] field, Object[] value) throws SQLException, InterruptedException {
        if (obj instanceof Employee) return new DAOEmploee().getEntityByField(field, value);
        else if (obj instanceof Employer) return new DAOEmployer().getEntityByField(field, value);
        else if (obj instanceof User) return new DAOUser().getEntityByField(field, value);
        else if (obj instanceof Company) return new DAOCompany().getEntityByField(field, value);
        else if (obj instanceof Interview) return new DAOInterview().getEntityByField(field, value);
        else if (obj instanceof Vacancy) return new DAOVacancy().getEntityByField(field, value);
        else if (obj instanceof InterviewResult) return new DAOInterviewResult().getEntityByField(field, value);
        return null;
    }
    
    public static Boolean deleteEntityByID(Entity obj) throws SQLException, InterruptedException {
        if (obj instanceof Employee) return new DAOEmploee().deleteEntityByField(obj);
        else if (obj instanceof Employer) return new DAOEmployer().deleteEntityByField(obj);
        else if (obj instanceof User) return new DAOUser().deleteEntityByField(obj);
        else if (obj instanceof Company) return new DAOCompany().deleteEntityByField(obj);
        else if (obj instanceof Interview) return new DAOInterview().deleteEntityByField(obj);
        else if (obj instanceof Vacancy) return new DAOVacancy().deleteEntityByField(obj);
        else if (obj instanceof InterviewResult) return new DAOInterviewResult().deleteEntityByField(obj);
        return false;
    }
    
    public static Boolean updateEntity(Entity obj) throws SQLException, InterruptedException {
        if (obj instanceof Employee) return new DAOEmploee().updateEntity(obj);
        else if (obj instanceof Employer) return new DAOEmployer().updateEntity(obj);
        else if (obj instanceof User) return new DAOUser().updateEntity(obj);
        else if (obj instanceof Company) return new DAOCompany().updateEntity(obj);
        else if (obj instanceof Interview) return new DAOInterview().updateEntity(obj);
        else if (obj instanceof Vacancy) return new DAOVacancy().updateEntity(obj);
        else if (obj instanceof InterviewResult) return new DAOInterviewResult().updateEntity(obj);
        return false;
    }
    
    public static Boolean createEntity(Entity obj) throws SQLException, InterruptedException {
        if (obj instanceof Employee) return new DAOEmploee().createEntity(obj);
        else if (obj instanceof Employer) return new DAOEmployer().createEntity(obj);
        else if (obj instanceof User) return new DAOUser().createEntity(obj);
        else if (obj instanceof Company) return new DAOCompany().createEntity(obj);
        else if (obj instanceof Interview) return new DAOInterview().createEntity(obj);
        else if (obj instanceof Vacancy) return new DAOVacancy().createEntity(obj);
        else if (obj instanceof InterviewResult) return new DAOInterviewResult().createEntity(obj);
        return false;
    }

    public static int getCount(Entity obj) throws SQLException, InterruptedException {
        if (obj instanceof Employee) return new DAOEmploee().getCount();
        else if (obj instanceof Employer) return new DAOEmployer().getCount();
        else if (obj instanceof User) return new DAOUser().getCount();
        else if (obj instanceof Company) return new DAOCompany().getCount();
        else if (obj instanceof Interview) return new DAOInterview().getCount();
        else if (obj instanceof Vacancy) return new DAOVacancy().getCount();
        else if (obj instanceof InterviewResult) return new DAOInterviewResult().getCount();
        return 0;
    }

    public static ArrayList<Entity> searchEntities(Entity obj, String field, String value) throws SQLException, InterruptedException {
        if (obj instanceof Employee) return new DAOEmploee().searchEntities(field, value);
        else if (obj instanceof Employer) return new DAOEmployer().searchEntities(field, value);
        else if (obj instanceof User) return new DAOUser().searchEntities(field, value);
        else if (obj instanceof Company) return new DAOCompany().searchEntities(field, value);
        else if (obj instanceof Interview) return new DAOInterview().searchEntities(field, value);
        else if (obj instanceof Vacancy) return new DAOVacancy().searchEntities(field, value);
        else if (obj instanceof InterviewResult) return new DAOInterviewResult().searchEntities(field, value);
        return null;
    }
}
