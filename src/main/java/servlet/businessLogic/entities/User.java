package servlet.businessLogic.entities;

import java.io.Serializable;
import java.sql.Timestamp;

public class User extends Entity implements Serializable {
    private String login;
    private String password;
    private String surname;
    private String name;
    private String role;
    private Integer isActive;
    private Timestamp dateOfRegistration;

    public User() {
        super();
        this.login = "";
        this.password = "";
        this.surname = "";
        this.name = "";
        this.role = "";
        this.isActive = 1;
        this.dateOfRegistration = null;
    }

    public User(String login, String password, String surname, String name, String role) {
        this.login = login;
        this.password = password;
        this.surname = surname;
        this.name = name;
        this.role = role;
    }

    @Override
    public void setDefault() {
        super.setDefault();
        if (this.login == null) this.login = "";
        if (this.password == null) this.password = "";
        if (this.surname == null) this.surname = "";
        if (this.name == null) this.name = "";
        if (this.role == null) this.role = "";
        if (this.isActive == null) this.isActive = 1;
        if (this.dateOfRegistration == null) this.dateOfRegistration = null;
    }

    public Integer getId() {
        return super.getID();
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public Timestamp getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public void setDateOfRegistration(Timestamp dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }
}
