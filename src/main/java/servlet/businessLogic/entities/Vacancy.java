package servlet.businessLogic.entities;


import java.io.Serializable;
import java.sql.Timestamp;

public class Vacancy extends Entity implements Serializable {
    private Integer idemployee;
    private String position;
    private Double salary;
    private String salaryCurrency;
    private String requirements;
    private Timestamp timeAdded;

    public Vacancy() {
        super();
        this.idemployee = 0;
        this.position = "";
        this.salary = 0.;
        this.salaryCurrency = "";
        this.requirements = "";
        this.timeAdded = new Timestamp(0, 0, 0, 0, 0, 0, 0);
    }

    @Override
    public void setDefault() {
        super.setDefault();
        if (this.idemployee == null) this.idemployee = 0;
        if (this.position == null) this.position = "";
        if (this.salary == null) this.salary = 0.;
        if (this.salaryCurrency == null) this.salaryCurrency = "";
        if (this.requirements == null) this.requirements = "";
        if (this.timeAdded == null) this.timeAdded = new Timestamp(0, 0, 0, 0, 0, 0, 0);
    }

    public Integer getId() {
        return super.getID();
    }

    public Integer getIdEmploee() {
        return idemployee;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getSalaryCurrency() {
        return salaryCurrency;
    }

    public void setSalaryCurrency(String salaryCurrency) {
        this.salaryCurrency = salaryCurrency;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public Timestamp getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(Timestamp timeAdded) {
        this.timeAdded = timeAdded;
    }

    public void setIdemployee(Integer idemployee) {
        this.idemployee = idemployee;
    }
}