package servlet.businessLogic.entities;

import java.io.Serializable;

public class Company extends Entity implements Serializable {

    public String name;
    public String address;
    public Integer iduser;

    public Company() {
        super();
        this.iduser = 0;
        this.name = "";
        this.address = "";
    }

    @Override
    public void setDefault() {
        super.setDefault();
        if (this.iduser == null) this.iduser = 0;
        if (this.name == null) this.name = "";
        if (this.address == null) this.address = "";
    }

    public Integer getId() {
        return super.getID();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }
}