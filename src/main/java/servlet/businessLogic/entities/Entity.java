package servlet.businessLogic.entities;

import java.io.Serializable;

public abstract class Entity implements Serializable {
    private Integer ID;

    public Entity() {
        this.ID = 0;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setDefault() {
        if (this.ID == null) this.ID = 0;
    }
}
