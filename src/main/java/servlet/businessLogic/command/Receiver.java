package servlet.businessLogic.command;

import org.xml.sax.SAXException;
import servlet.businessLogic.entities.*;
import servlet.businessLogic.logic.Authorization;
import servlet.businessLogic.logic.DataGet;
import servlet.businessLogic.logic.Registration.*;
import servlet.dao.DAOUser;
import servlet.dao.file_work.DOMParser;
import servlet.presentationLayer.View;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Receiver {
    HttpServletRequest req;
    HttpServletResponse resp;
    HttpSession session;

    public Receiver(HttpServletRequest req, HttpServletResponse resp) {
        this.req = req;
        this.resp = resp;
        session = req.getSession(true);
    }

    public void action(TypeCommand cmd) throws SQLException, InterruptedException, ServletException, IOException, ParserConfigurationException, SAXException {
        switch (cmd) {
            case AUTHORIZATION: {
                Authorization authorization = new Authorization(req.getParameter("password"), req.getParameter("login"));
                if (authorization.checkValidityLogin() && authorization.checkValidityPassword() && authorization.checkMatchInDB()) {
                    authorization.getUserInDB();
                    User user = (User) authorization.getUserInDB();
                    if (user.getIsActive() != 0) {
                        req.setAttribute("failed", "blockedAccount");
                        sendPage("authorization");
                        return;
                    }
                    Cookie[] cookies = req.getCookies();
                    Cookie cookie = new Cookie("password", req.getParameter("password"));
                    resp.addCookie(cookie);
                    Cookie cookie1 = new Cookie("login", req.getParameter("login"));
                    resp.addCookie(cookie1);
                    session.setAttribute("user", user);
                    String role = null;
                    if (user.getRole().equals("соискатель")) {
                        role = "user";
                    } else if (user.getRole().equals("сотрудник")) {
                        role = "employer";
                    } else if (user.getRole().equals("администратор")) {
                        role = "administrator";
                    } else {
                        sendError();
                        return;
                    }
                    session.setAttribute("role", role);
                    session.setAttribute("userID", user.getId());
                    pushDataToRequest(session.getAttribute("role").toString(), "content");
                    req.getRequestDispatcher("jsp//" + session.getAttribute("role") + ".jsp").forward(req, resp);
                } else {
                    req.setAttribute("failed", "wrongData");
                    sendPage("authorization");
                }
                break;
            }
            case REGISTRATION: {
                if (req.getParameter("aim") != null) {
                    switch (req.getParameter("aim").toString()) {
                        case "user": {
                            Registration registration = new Registration(req.getParameter("password"), req.getParameter("login"), req.getParameter("email"), req.getParameter("name"), req.getParameter("surname"), "соискатель");
                            if (!registration.register()) {
                                req.setAttribute("failed", "true");
                            } else {
                                req.setAttribute("failed", "false");
                            }
                            sendPage("registration");
                            break;
                        }
                        case "company": {
                            CompanyRegistration companyRegistration = new CompanyRegistration(req.getParameter("name"), req.getParameter("address"), (User) session.getAttribute("user"));
                            if (!companyRegistration.register()) {
                                req.setAttribute("failed", "true");
                            } else {
                                req.setAttribute("failed", "false");
                            }
                            sendPage("allCompanies");
                            break;
                        }
                        case "employer": {
                            Registration registration = new Registration(req.getParameter("password"), req.getParameter("login"), "", req.getParameter("name"), req.getParameter("surname"), "сотрудник", Integer.parseInt(req.getParameter("itemID")));
                            if (!registration.register()) {
                                req.setAttribute("failed", "true");
                            } else {
                                req.setAttribute("failed", "false");
                            }
                            sendPage("employers");
                            break;
                        }
                        case "administrator": {
                            Registration registration = new Registration(req.getParameter("password"), req.getParameter("login"), "", "", "", "администратор");
                            if (!registration.register()) {
                                req.setAttribute("failed", "true");
                            } else {
                                req.setAttribute("failed", "false");
                            }
                            sendPage("administrator");
                            break;
                        }
                        case "vacancy": {
                            VacancyRegistration vacancyRegistration = new VacancyRegistration(req.getParameter("position"), req.getParameter("salary"), req.getParameter("salary_currency"), req.getParameter("other"), (User) session.getAttribute("user"));
                            if (!vacancyRegistration.register()) {
                                req.setAttribute("failed", "true");
                            } else {
                                req.setAttribute("failed", "false");
                            }
                            sendPage("employer");
                            break;
                        }
                        case "interview": {
                            InterviewRegistration interviewRegistration = new InterviewRegistration(Integer.valueOf(req.getParameter("itemID")), (User) session.getAttribute("user"));
                            if (!interviewRegistration.register()) {
                                req.setAttribute("failed", "true");
                            } else {
                                req.setAttribute("failed", "false");
                            }
                            sendPage("allInterviews");
                            break;
                        }
                        case "interviewResult": {
                            InterviewResultRegistration interviewResultRegistration = new InterviewResultRegistration(Integer.valueOf(req.getParameter("itemID")), Double.valueOf(req.getParameter("mark")), String.valueOf(req.getParameter("recall")));
                            if (!interviewResultRegistration.register()) {
                                req.setAttribute("failed", "true");
                            } else {
                                req.setAttribute("failed", "false");
                            }
                            sendPage("allInterviews");
                            break;
                        }
                        default: {
                            sendError();
                            break;
                        }
                    }
                } else sendError();
                break;
            }
            case SEARCH: {
                session.setAttribute("searchData", req.getParameter("searchData"));
                if (req.getParameter("type").toString().equals("1") || req.getParameter("type").toString().equals("2")) {
                    switch (req.getParameter("type")) {
                        case "1": {
                            req.setAttribute("type", "1");
                            ArrayList<Entity> arrayList = DataGet.searchEntities(new Vacancy(), "position", req.getParameter("searchData").toString());
                            session.setAttribute("allData", arrayList);
                            break;
                        }
                        case "2": {
                            req.setAttribute("type", "2");
                            ArrayList<Entity> arrayList = DataGet.searchEntities(new Company(), "name", req.getParameter("searchData").toString());
                            session.setAttribute("allData", arrayList);
                            break;
                        }
                    }
                    sendPage("searchResult");
                } else sendStartPage();
                break;
            }
            case CLOSE: {
                session.setAttribute("role", null);
                sendStartPage();
                break;
            }
            case START_PAGE: {
                sendStartPage();
                break;
            }
            case ERROR: {
                sendError();
                break;
            }
            case MORE_DATA: {
                if (req.getParameter("type") != null) {
                    if (session.getAttribute("role")!=null && session.getAttribute("role").equals("administrator")) {
                        switch (req.getParameter("type")) {
                            case "allUsers": {
                                if (req.getParameter("aim") == null) {
                                    req.setAttribute("data", DataGet.getALL(new User()));
                                } else {
                                    String[] users = null;
                                    String[] fields = {"role"};
                                    if (req.getParameter("aim").equals("user")) {
                                        users = new String[]{"соискатель"};
                                    } else if (req.getParameter("aim").equals("employer")) {
                                        users = new String[]{"сотрудник"};
                                    } else if (req.getParameter("aim").equals("admin")) {
                                        users = new String[]{"администратор"};
                                    }
                                    req.setAttribute("data", DataGet.getALLByField(new User(), fields, users));
                                }
                                sendPage("adminReduct");
                                break;
                            }
                            case "allCompanies": {
                                req.setAttribute("aim", "company");
                                req.setAttribute("data", DataGet.getALL(new Company()));
                                sendPage("adminReduct");
                                break;
                            }
                        }
                        break;
                    }
                    switch (req.getParameter("type")) {
                        case "vacancy":
                        case "company":
                        case "interview": {
                            if (req.getParameter("type").equals("company")) {
                                String[] field = {"idcompany"};
                                Object[] value = {req.getParameter("itemID")};
                                ArrayList<Entity> arrayListEmployer = DataGet.getALLByField(new Employer(), field, value);
                                ArrayList<Entity> arrayList = new ArrayList<>();
                                for (Entity emp : arrayListEmployer) {
                                    Employer tmp = (Employer) emp;
                                    String[] fieldV = {"idemployee"};
                                    Object[] valueV = {tmp.getIdemployee()};
                                    arrayList.addAll(DataGet.getALLByField(new Vacancy(), fieldV, valueV));
                                }
                                Company company = View.getCompanyById(Integer.parseInt(req.getParameter("itemID")));
                                req.setAttribute("creator", View.getUserByID(company.getIduser()));
                                req.setAttribute("allData", arrayList);
                            }
                            req.setAttribute("id", req.getParameter("itemID"));
                            sendPage(req.getParameter("type"));
                            break;
                        }
                        case "employers": {
                            req.setAttribute("id", req.getParameter("itemID"));
                            sendPage("employers");
                            break;
                        }
                        case "user": {
                            req.setAttribute("id", req.getParameter("itemID"));
                            sendPage("profile");
                            break;
                        }
                        default: {
                            sendError();
                            break;
                        }
                    }
                } else sendError();
                break;
            }
            case DELETE: {
                if (req.getParameter("type") != null) {
                    if (session.getAttribute("role").equals("user") || session.getAttribute("role").equals("administrator")) {
                        switch (req.getParameter("type")) {
                            case "user": {
                                User user = new User();
                                Employee employee = new Employee();
                                employee.setIdapplicant(Integer.parseInt(session.getAttribute("userID").toString()));
                                user.setID(Integer.parseInt(session.getAttribute("userID").toString()));
                                DataGet.deleteEntityByID(employee);
                                DataGet.deleteEntityByID(user);
                                session.setAttribute("role", null);
                                sendStartPage();
                                break;
                            }
                            case "interview": {
                                Interview interview = View.getInterviewByID(Integer.parseInt(req.getParameter("itemID").toString()));
                                DataGet.deleteEntityByID(interview);
                                sendStartPage();
                                break;
                            }
                            case "interviewResult": {
                                InterviewResult interviewResult = View.getInterviewResultByID(Integer.parseInt(req.getParameter("itemID")));
                                DataGet.deleteEntityByID(interviewResult);
                                sendStartPage();
                                break;
                            }
                            case "company": {
                                Company company = View.getCompanyById(Integer.parseInt(req.getParameter("itemID")));
                                DataGet.deleteEntityByID(company);
                                sendStartPage();
                                break;
                            }
                            case "employer": {
                                User user = new User();
                                user.setID(Integer.parseInt(req.getParameter("itemID").toString()));
                                Employer employer = View.getEmployerByID(Integer.parseInt(req.getParameter("itemID")));
                                DataGet.deleteEntityByID(employer);
                                DataGet.deleteEntityByID(user);
                                sendStartPage();
                                break;
                            }
                            case "otherUser": {
                                User user = new User();
                                user.setID(Integer.parseInt(req.getParameter("itemID").toString()));
                                DataGet.deleteEntityByID(user);
                                sendStartPage();
                                break;
                            }
                            default: {
                                sendError();
                                return;
                            }
                        }
                    } else if (session.getAttribute("role").equals("employer")) {
                        switch (req.getParameter("type")) {
                            case "interview": {
                                Interview interview = View.getInterviewByID(Integer.parseInt(req.getParameter("itemID").toString()));
                                DataGet.deleteEntityByID(interview);
                                sendPage("vacancy");
                                break;
                            }
                            case "vacancy": {
                                Vacancy vacancy = View.getVacancyByID(Integer.parseInt(req.getParameter("itemID").toString()));
                                DataGet.deleteEntityByID(vacancy);
                                sendPage("employer");
                                break;
                            }
                            case "interviewResult": {
                                InterviewResult interviewResult = View.getInterviewResultByID(Integer.parseInt(req.getParameter("itemID")));
                                DataGet.deleteEntityByID(interviewResult);
                                sendStartPage();
                                break;
                            }
                            default: {
                                sendError();
                                return;
                            }
                        }
                    } else sendError();
                } else sendError();
                break;
            }
            case UPDATE: {
                if (req.getParameter("type") != null) {
                    if (session.getAttribute("role") != null) {
                        switch (session.getAttribute("role").toString()) {
                            case "user": {
                                if (req.getParameter("type").equals("login") || req.getParameter("type").equals("password") || req.getParameter("type").equals("name") || req.getParameter("type").equals("surname")) {
                                    User user = View.getUserByID(Integer.parseInt(session.getAttribute("userID").toString()));
                                    user.setDefault();
                                    switch (req.getParameter("type")) {
                                        case "login": {
                                            user.setLogin(req.getParameter(req.getParameter("type")));
                                            Registration registration1 = new Registration(user.getLogin(), "", "", "", "", "");
                                            if (!registration1.checkMatchInDB()) {
                                                DataGet.updateEntity(user);
                                                session.setAttribute("user", View.getUserByID(Integer.parseInt(session.getAttribute("userID").toString())));
                                            }
                                            break;
                                        }
                                        case "password": {
                                            user.setPassword(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        case "name": {
                                            user.setName(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        case "surname": {
                                            user.setSurname(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        default: {
                                            sendError();
                                            return;
                                        }
                                    }
                                    DataGet.updateEntity(user);
                                    session.setAttribute("user", View.getUserByID(Integer.parseInt(session.getAttribute("userID").toString())));
                                } else {
                                    Employee employee = View.getEmployeeById(Integer.parseInt(session.getAttribute("userID").toString()));
                                    employee.setDefault();
                                    switch (req.getParameter("type")) {
                                        case "birthday": {
                                            employee.setdateOfBrithday(Date.valueOf(req.getParameter(req.getParameter("type"))));
                                            break;
                                        }
                                        case "email": {
                                            employee.setEmail(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        case "number": {
                                            employee.setTelephoneNumber(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        case "sex": {
                                            employee.setSex(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        case "education": {
                                            employee.setEducation(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        case "other": {
                                            employee.setOther(req.getParameter(req.getParameter("type")));
                                            break;
                                        }
                                        default: {
                                            sendError();
                                            return;
                                        }
                                    }
                                    DataGet.updateEntity(employee);
                                    switch (req.getParameter("type")) {
                                        case "birthday":
                                        case "email":
                                        case "number":
                                        case "sex": {
                                            sendPage("userProfile");
                                            break;
                                        }
                                        case "education":
                                        case "other": {
                                            sendPage("resume");
                                            break;
                                        }
                                    }
                                }
                            }
                            case "employer": {
                                switch (req.getParameter("type")) {
                                    case "interviewTime": {
                                        Interview interview = View.getInterviewByID(Integer.parseInt(req.getParameter("itemID").toString()));
                                        req.setAttribute("id", interview.getIdvacancy());
                                        interview.setDefault();
                                        try {
                                            interview.setTimeOfInterview(Timestamp.valueOf(req.getParameter("date") + " " + req.getParameter("time") + ":00"));
                                        } catch (Exception e) {
                                            req.setAttribute("failed", "true");
                                            sendPage("vacancy");
                                            break;
                                        }
                                        DataGet.updateEntity(interview);
                                        req.setAttribute("failed", "false");
                                        sendPage("vacancy");
                                        break;
                                    }
                                    default: {
                                        sendError();
                                    }
                                }
                                break;
                            }
                            case "administrator": {
                                User user = View.getUserByID(Integer.parseInt(req.getParameter("itemID").toString()));
                                user.setDefault();
                                switch (req.getParameter("type")) {
                                    case "isActive": {
                                        user.setIsActive(Integer.valueOf(req.getParameter("isActive")));
                                        DataGet.updateEntity(user);
                                        req.setAttribute("data", DataGet.getALL(new User()));
                                        sendPage("adminReduct");
                                        break;
                                    }
                                }
                                break;
                            }
                            default: {
                                sendError();
                                return;
                            }
                        }
                    } else sendError();
                } else sendError();
                break;
            }
            case CHANGE_LANGUAGE: {
                session.setAttribute("language", req.getParameter("type"));
                pushDataToSession("advertisement", "advertisement");
                pushDataToSession("headers", "headers");
                sendStartPage();
                break;
            }
            case SEND_PAGE: {
                sendPage(req.getParameter("page"));
                break;
            }
            default: {
                sendError();
            }
        }

    }

    void sendError() throws ServletException, IOException, ParserConfigurationException, SAXException {
        pushDataToRequest("error", "content");
        req.getRequestDispatcher("jsp//error.jsp").forward(req, resp);
    }

    void sendPage(String href) throws ServletException, IOException, ParserConfigurationException, SAXException {
        if (!href.isEmpty()) {
            pushDataToSession(href, "content");
            req.getRequestDispatcher("jsp//" + href + ".jsp").forward(req, resp);
        } else sendError();
    }

    void pushDataToSession(String fileName, String attributeName) throws ServletException, IOException, ParserConfigurationException, SAXException {
        DOMParser domParserForAdvertisement = new DOMParser("src//main//resources//xml//" + session.getAttribute("language") + "//" + fileName + ".xml");
        req.getSession().setAttribute(attributeName, domParserForAdvertisement.getValues());
    }

    void pushDataToRequest(String fileName, String attributeName) throws ServletException, IOException, ParserConfigurationException, SAXException {
        DOMParser domParserForAdvertisement = new DOMParser("src//main//resources//xml//" + session.getAttribute("language") + "//" + fileName + ".xml");
        req.setAttribute(attributeName, domParserForAdvertisement.getValues());
    }

    void sendStartPage() throws ParserConfigurationException, SAXException, ServletException, IOException {
        if (session.getAttribute("role") != null) {
            pushDataToRequest(session.getAttribute("role").toString(), "content");
            req.getRequestDispatcher("jsp//" + session.getAttribute("role") + ".jsp").forward(req, resp);
        } else {
            pushDataToRequest("index", "content");
            try {
                session.setAttribute("quantityVacancy", DataGet.getCount(new Vacancy()));
                session.setAttribute("quantityEmployee", DataGet.getCount(new Employee()));
                session.setAttribute("quantityCompany", DataGet.getCount(new Company()));
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            req.getRequestDispatcher("jsp//index.jsp").forward(req, resp);
        }
    }
}
