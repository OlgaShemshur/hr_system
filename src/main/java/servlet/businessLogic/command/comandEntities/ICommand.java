package servlet.businessLogic.command.comandEntities;

import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;

public interface ICommand {
    void execute() throws SQLException, InterruptedException, ServletException, IOException, ParserConfigurationException, SAXException;
}
