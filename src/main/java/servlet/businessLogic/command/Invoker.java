package servlet.businessLogic.command;

import org.xml.sax.SAXException;
import servlet.businessLogic.command.comandEntities.ICommand;

import javax.servlet.ServletException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;

public class Invoker {
    private ICommand mCommand;
    public Invoker(ICommand command) {
        mCommand = command;
    }
    public void invokeCommand() throws InterruptedException, SQLException, ServletException, IOException, ParserConfigurationException, SAXException {
        mCommand.execute();
    }
}
