package servlet.presentationLayer;

import org.xml.sax.SAXException;
import servlet.businessLogic.command.*;
import servlet.businessLogic.command.comandEntities.ICommand;
import servlet.dao.file_work.DOMParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.sql.SQLException;

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession(true);
        increaseCounter(session);
        checkLanguageAttribute(session);
        Receiver receiver = new Receiver(req, resp);
        Client client = new Client(receiver);
        ICommand command = null;
        if (req.getParameter("page") == null) {
            command = client.initCommand(TypeCommand.START_PAGE);
        } else command = client.initCommand(TypeCommand.SEND_PAGE);
        Invoker invoker = new Invoker(command);
        try {
            invoker.invokeCommand();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession(true);
        increaseCounter(session);
        checkLanguageAttribute(session);
        Receiver receiver = new Receiver(req, resp);
        Client client = new Client(receiver);
        ICommand command = null;
        if (req.getParameter("action") != null) {
            switch (req.getParameter("action")) {
                case "authorization": {
                    command = client.initCommand(TypeCommand.AUTHORIZATION);
                    break;
                }
                case "registration": {
                    command = client.initCommand(TypeCommand.REGISTRATION);
                    break;
                }
                case "searchItems": {
                    command = client.initCommand(TypeCommand.SEARCH);
                    break;
                }
                case "endSession": {
                    command = client.initCommand(TypeCommand.CLOSE);
                    break;
                }
                case "moreInfo": {
                    command = client.initCommand(TypeCommand.MORE_DATA);
                    break;
                }
                case "delete": {
                    command = client.initCommand(TypeCommand.DELETE);
                    break;
                }
                case "update": {
                    command = client.initCommand(TypeCommand.UPDATE);
                    break;
                }
                case "language": {
                    command = client.initCommand(TypeCommand.CHANGE_LANGUAGE);
                    break;
                }
                default: {
                    command = client.initCommand(TypeCommand.ERROR);
                }
            }
        } else {
            command = client.initCommand(TypeCommand.ERROR);
        }
        try {
            Invoker invoker = new Invoker(command);
            invoker.invokeCommand();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void increaseCounter(HttpSession session) {
        Integer counter = (Integer) session.getAttribute("counter");
        if (counter == null) {
            session.setAttribute("counter", 1);
        } else {
            counter++;
            session.setAttribute("counter", counter);
        }
    }

    private void checkLanguageAttribute(HttpSession session) {
        if (session.getAttribute("language") == null) {
            session.setAttribute("language", "ru");
            DOMParser domParserForAdvertisement = null;
            try {
                domParserForAdvertisement = new DOMParser("src//main//resources//xml//" + session.getAttribute("language") + "//advertisement.xml");
                session.setAttribute("advertisement", domParserForAdvertisement.getValues());
                DOMParser domParserForHeaders = new DOMParser("src//main//resources//xml//" + session.getAttribute("language") + "//headers.xml");
                session.setAttribute("headers", domParserForHeaders.getValues());
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}