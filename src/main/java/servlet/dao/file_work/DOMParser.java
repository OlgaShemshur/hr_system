package servlet.dao.file_work;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Created by Olga on 21.11.2016
 */
public class DOMParser {

    private Document doc;
    private ArrayList<Node> nodes;
    private ArrayList<String> values;

    public DOMParser(String xmlFilename) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        f.setValidating(false);
        DocumentBuilder builder = f.newDocumentBuilder();
        XMLvalidation xmlFile = new XMLvalidation(xmlFilename);
        doc = builder.parse(xmlFile.getXMLFile());
        nodes = new ArrayList<>();
        values = new ArrayList<>();
        read(doc);
        getValuesOfElements();
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public Document getDoc() {
        return doc;
    }

    public void read(Node node) {
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node childNode = list.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                boolean flag = true;
                NodeList nodeListOfChild = childNode.getChildNodes();
                for (int y = 0; y < nodeListOfChild.getLength(); y++) {
                    if (nodeListOfChild.item(y).getNodeType() == Node.ELEMENT_NODE) flag = false;
                }
                if (flag) nodes.add(childNode);
            }
            read(list.item(i));
        }
    }

    public void getValuesOfElements() throws IOException, SAXException, ParserConfigurationException {
        for (Node i : nodes) {
            NodeList nodeList = i.getChildNodes();
            for (int j = 0; j < nodeList.getLength(); j++) {
                if (nodeList.item(j).getNodeValue() != null) values.add(nodeList.item(j).getNodeValue());
            }
        }
    }
}
