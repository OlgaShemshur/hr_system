package servlet.dao;

import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.Interview;
import servlet.businessLogic.entities.Vacancy;

import java.sql.*;
import java.util.ArrayList;

public class DAOVacancy implements DAOInterface {
    @Override
    public boolean isExist() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        try  {
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, "vacancy",
                    new String[] {"TABLE"});
            while (res.next()) {
                return true;
            }
            return false;
        }
        catch (SQLException e) {
            return false;
        }
        finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM vacancy where ";
        for(int i=0; i<field.length; i++){
            if(value[i] instanceof Integer || value[i] instanceof Float) sql += field[i] + " = " + value[i];
            else sql += field[i] + " = '" + value[i] + "'";
            if(i+1 != field.length) sql = " AND ";
        }
        sql += ";";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Vacancy vacancy = new Vacancy();
            vacancy.setID(rs.getInt("id"));
            vacancy.setIdemployee(rs.getInt("idemployee"));
            vacancy.setPosition(rs.getString("position"));
            vacancy.setSalary(rs.getDouble("salary"));
            vacancy.setSalaryCurrency(rs.getString("salary_currency"));
            vacancy.setRequirements(rs.getString("requirements"));
            vacancy.setTimeAdded(rs.getTimestamp("time_added"));
            vacancy.setDefault();
            arrayList.add(vacancy);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "DELETE FROM vacancy where id = " + entity.getID() + ";";
        DAOInterview daoInterview = new DAOInterview();
        Interview ir = new Interview();
        ir.setIdvacancy(entity.getID());
        daoInterview.deleteEntityByVacancy(ir);
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }
    
    public boolean deleteEntityByEmploer(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        Vacancy vacancy = (Vacancy)entity;
        String sql = "DELETE FROM vacancy where idemployee = " + vacancy.getIdEmploee()+ ";";
        DAOInterview daoInterview = new DAOInterview();
        Interview ir = new Interview();
        ir.setIdvacancy(entity.getID());
        daoInterview.deleteEntityByVacancy(ir);
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }
    
    @Override
    public ArrayList<Entity> getAll() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM vacancy;";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Vacancy vacancy = new Vacancy();
            vacancy.setID(rs.getInt("id"));
            vacancy.setIdemployee(rs.getInt("idemployee"));
            vacancy.setPosition(rs.getString("position"));
            vacancy.setSalary(rs.getDouble("salary"));
            vacancy.setSalaryCurrency(rs.getString("salary_currency"));
            vacancy.setRequirements(rs.getString("requirements"));
            vacancy.setTimeAdded(rs.getTimestamp("time_added"));
            vacancy.setDefault();
            arrayList.add(vacancy);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean createEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        Vacancy vacancy = (Vacancy)entitiy;
        String sql = "";
        if (vacancy.getSalary() != 0) {
            sql = "INSERT INTO vacancy (idemployee , position, salary, salary_currency, requirements) VALUES(" +
                    vacancy.getIdEmploee() + ", '" + vacancy.getPosition() + "', " + vacancy.getSalary() + ", '" + vacancy.getSalaryCurrency() + "', '" + vacancy.getRequirements() + "');";
        } else {
            sql = "INSERT INTO vacancy (idemployee , position, salary_currency, requirements) VALUES(" +
                    vacancy.getIdEmploee() + ", '" + vacancy.getPosition() + "', '" + vacancy.getSalaryCurrency() + "', '" + vacancy.getRequirements() + "');";
        }
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public boolean updateEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Vacancy  vacancy = (Vacancy)entitiy;
            String sql = "UPDATE vacancy set position = '" + vacancy.getPosition()+ "', salary = " + vacancy.getSalary()+
                    ", salary_currency = '" + vacancy.getSalaryCurrency()+ "', requirements = '" + vacancy.getRequirements()+ "' where id = " + vacancy.getID() + "; ";
           statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public int getCount() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT COUNT(\"id\") AS 'COUNT' FROM vacancy;";
        ResultSet rs = statement.executeQuery(sql);
        int count = 0;
        while(rs.next()){
            count = rs.getInt("COUNT");
        }
        ConnectionPool.closeConnection(connection);
        return count;
    }

    @Override
    public ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM vacancy where "+ field +" LIKE '%" + value + "%';";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Vacancy vacancy = new Vacancy();
            vacancy.setID(rs.getInt("id"));
            vacancy.setIdemployee(rs.getInt("idemployee"));
            vacancy.setPosition(rs.getString("position"));
            vacancy.setSalary(rs.getDouble("salary"));
            vacancy.setSalaryCurrency(rs.getString("salary_currency"));
            vacancy.setRequirements(rs.getString("requirements"));
            vacancy.setTimeAdded(rs.getTimestamp("time_added"));
            vacancy.setDefault();
            arrayList.add(vacancy);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
}
