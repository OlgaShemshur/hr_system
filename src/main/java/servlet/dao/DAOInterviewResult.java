package servlet.dao;

import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.InterviewResult;

import java.sql.*;
import java.util.ArrayList;

public class DAOInterviewResult implements DAOInterface {
    @Override
    public boolean isExist() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        try  {
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, "interview_result",
                    new String[] {"TABLE"});
            while (res.next()) {
                return true;
            }
            return false;
        }
        catch (SQLException e) {
            return false;
        }
        finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM interview_result where ";
        for(int i=0; i<field.length; i++){
            if(value[i] instanceof Integer || value[i] instanceof Float) sql += field[i] + " = " + value[i];
            else sql += field[i] + " = '" + value[i] + "'";
            if(i+1 != field.length) sql = " AND ";
        }
        sql += ";";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            InterviewResult interviewResult = new InterviewResult();
            interviewResult.setInterviewid(rs.getInt("idinterview"));
            interviewResult.setRecall(rs.getString("recall"));
            interviewResult.setTimeAdded(rs.getTimestamp("time_added"));
            interviewResult.setAverageMark(rs.getDouble("average_mark"));
            interviewResult.setDefault();
            arrayList.add(interviewResult);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        InterviewResult ir = (InterviewResult)entity;
        String sql = "DELETE FROM interview_result where idinterview = " + ir.getInterviewid()+ ";";
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }
    
    @Override
    public ArrayList<Entity> getAll() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM interview_result where;";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            InterviewResult interviewResult = new InterviewResult();
            interviewResult.setInterviewid(rs.getInt("idinterview"));
            interviewResult.setRecall(rs.getString("recall"));
            interviewResult.setTimeAdded(rs.getTimestamp("time_added"));
            interviewResult.setAverageMark(rs.getDouble("average_mark"));
            interviewResult.setDefault();
            arrayList.add(interviewResult);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean createEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            InterviewResult interviewResult = (InterviewResult)entitiy;
            String sql = "INSERT INTO interview_result (idinterview, recall, average_mark) VALUES ("
                    + interviewResult.getInterviewid() + ", '" + interviewResult.getRecall()+
                    "', '" + interviewResult.getAverageMark()+ "');";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public boolean updateEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            InterviewResult interviewResult = (InterviewResult)entitiy;
            String sql = "UPDATE interview_result set recall = '" + interviewResult.getRecall()+ 
                    "', average_mark = '" + interviewResult.getAverageMark()+ "' where id = " + 
                    interviewResult.getID() + ";";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public int getCount() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT COUNT(\"id\") AS 'COUNT' FROM interview_result;";
        ResultSet rs = statement.executeQuery(sql);
        int count = 0;
        while(rs.next()){
            count = rs.getInt("COUNT");
        }
        ConnectionPool.closeConnection(connection);
        return count;
    }

    @Override
    public ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM interview_result where UPPER('"+ field +"') LIKE UPPER('%\" + value +\"%');";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            InterviewResult interviewResult = new InterviewResult();
            interviewResult.setInterviewid(rs.getInt("idinterview"));
            interviewResult.setRecall(rs.getString("recall"));
            interviewResult.setTimeAdded(rs.getTimestamp("time_added"));
            interviewResult.setAverageMark(rs.getDouble("average_mark"));
            interviewResult.setDefault();
            arrayList.add(interviewResult);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
}
