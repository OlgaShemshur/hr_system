package servlet.dao;

import servlet.businessLogic.entities.Employee;
import servlet.businessLogic.entities.Employer;
import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.User;

import java.sql.*;
import java.util.ArrayList;

public class DAOUser implements DAOInterface {
    @Override
    public boolean isExist() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        try {
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, "user",
                    new String[]{"TABLE"});
            while (res.next()) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            return false;
        } finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        if (field.length < 1 || value.length < 1 || field.length != value.length) return null;
        String sql = "SELECT * FROM user where ";
        for (int i = 0; i < field.length; i++) {
            if (value[i] instanceof Integer || value[i] instanceof Float) sql += field[i] + " = " + value[i];
            else sql += field[i] + " = '" + value[i] + "'";
            if (i + 1 != field.length) sql += " AND ";
        }
        sql += ";";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while (rs.next()) {
            User user = new User();
            user.setID(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
            user.setPassword(rs.getString("password"));
            user.setSurname(rs.getString("surname"));
            user.setName(rs.getString("name"));
            user.setRole(rs.getString("role"));
            user.setIsActive(rs.getInt("isactive"));
            user.setDateOfRegistration(rs.getTimestamp("registration_time"));
            user.setDefault();
            arrayList.add(user);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "DELETE FROM user where id = " + entity.getID() + ";";
        DAOEmployer daoEmployer = new DAOEmployer();
        Employer employer = new Employer();
        employer.setIdemployee(entity.getID());
        daoEmployer.deleteEntityByField(employer);
        DAOEmploee daoEmploee = new DAOEmploee();
        Employee employee = new Employee();
        employer.setIdemployee(entity.getID());
        daoEmployer.deleteEntityByField(employer);
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public ArrayList<Entity> getAll() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM user;";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while (rs.next()) {
            User user = new User();
            user.setID(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
            user.setPassword(rs.getString("password"));
            user.setSurname(rs.getString("surname"));
            user.setName(rs.getString("name"));
            user.setRole(rs.getString("role"));
            user.setIsActive(rs.getInt("isactive"));
            user.setDateOfRegistration(rs.getTimestamp("registration_time"));
            user.setDefault();
            arrayList.add(user);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean createEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        User user = (User) entitiy;
        String sql = "INSERT INTO user (login, password, surname, name, role) VALUES('" + user.getLogin() + "', '" + user.getPassword() + "', '" + user.getSurname() +
                "', '" + user.getName() + "', '" + user.getRole() + "');";
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public boolean updateEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        User user = (User) entitiy;
        String sql = "UPDATE user set login = '" + user.getLogin() + "', password = '" + user.getPassword() + "', surname = '" + user.getSurname() +
                "', name = '" + user.getName() + "', isactive = " + user.getIsActive() + " where id = " + user.getID() + "; ";
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public int getCount() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT COUNT(\"id\") AS 'COUNT' FROM user;";
        ResultSet rs = statement.executeQuery(sql);
        int count = 0;
        while(rs.next()){
            count = rs.getInt("COUNT");
        }
        ConnectionPool.closeConnection(connection);
        return count;
    }

    @Override
    public ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM user where UPPER('"+ field +"') LIKE UPPER('%\" + value +\"%');";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while (rs.next()) {
            User user = new User();
            user.setID(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
            user.setPassword(rs.getString("password"));
            user.setSurname(rs.getString("surname"));
            user.setName(rs.getString("name"));
            user.setRole(rs.getString("role"));
            user.setIsActive(rs.getInt("isactive"));
            user.setDateOfRegistration(rs.getTimestamp("registration_time"));
            user.setDefault();
            arrayList.add(user);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
}
