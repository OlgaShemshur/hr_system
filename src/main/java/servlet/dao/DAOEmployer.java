package servlet.dao;

import servlet.businessLogic.entities.Employer;
import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.Vacancy;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DAOEmployer implements DAOInterface{
    @Override
    public boolean isExist() throws SQLException, InterruptedException {
        return false;
    }

    @Override
    public ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM employee_data where ";
        for(int i=0; i<field.length; i++){
            if(value[i] instanceof Integer || value[i] instanceof Float) sql += field[i] + " = " + value[i];
            else sql += field[i] + " = '" + value[i] + "'";
            if(i+1 != field.length) sql = " AND ";
        }
        sql += ";";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Employer employer = new Employer();
            employer.setIdemployee(rs.getInt("idemployee"));
            employer.setIdcompany(rs.getInt("idcompany"));
            employer.setEmail(rs.getString("email"));
            employer.setTelephoneNumber(rs.getString("telephone_number"));
            employer.setOther(rs.getString("other"));
            employer.setDefault();
            arrayList.add(employer);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        Employer employer = (Employer)entity;
        String sql = "DELETE FROM employee_data where idemployee = " + employer.getIdemployee()+ ";";
        DAOVacancy daoVacancy = new DAOVacancy();
        Vacancy vacancy = new Vacancy();
        vacancy.setIdemployee(entity.getID());
        daoVacancy.deleteEntityByEmploer(vacancy);
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }
    
    public boolean deleteEntityByCompany(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        Employer employer = (Employer)entity;
        String sql = "DELETE FROM employee_data where idcompany = " + employer.getIdcompany() + ";";
        DAOVacancy daoVacancy = new DAOVacancy();
        Vacancy vacancy = new Vacancy();
        vacancy.setIdemployee(entity.getID());
        daoVacancy.deleteEntityByEmploer(vacancy);
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public boolean createEntity(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Employer employer = (Employer)entity;
            String sql = "INSERT INTO employee_data (idemployee, idcompany, email, telephone_number, other) VALUES(" 
                    + employer.getIdemployee() + ", " + employer.getIdcompany() +", '" + employer.getEmail()+
                    "', '" + employer.getTelephoneNumber() + "', '" + employer.getOther()+ "');";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public ArrayList<Entity> getAll() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM employee_data;";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Employer employer = new Employer();
            employer.setIdemployee(rs.getInt("idemployee"));
            employer.setIdcompany(rs.getInt("idcompany"));
            employer.setEmail(rs.getString("email"));
            employer.setTelephoneNumber(rs.getString("telephone_number"));
            employer.setOther(rs.getString("other"));
            employer.setDefault();
            arrayList.add(employer);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean updateEntity(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Employer employer = (Employer)entity;
            String sql = "UPDATE employee_data set email = '" + employer.getEmail()+
                    "', telephone_number = '" + employer.getTelephoneNumber()+ "', other = '" + employer.getOther()+ "' where id = " + employer.getIdemployee() + "; ";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public int getCount() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT COUNT(\"id\") AS 'COUNT' FROM employee_data;";
        ResultSet rs = statement.executeQuery(sql);
        int count = 0;
        while(rs.next()){
            count = rs.getInt("COUNT");
        }
        ConnectionPool.closeConnection(connection);
        return count;
    }

    @Override
    public ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM employee_data where UPPER('"+ field +"') LIKE UPPER('%\" + value +\"%');";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Employer employer = new Employer();
            employer.setIdemployee(rs.getInt("idemployee"));
            employer.setIdcompany(rs.getInt("idcompany"));
            employer.setEmail(rs.getString("email"));
            employer.setTelephoneNumber(rs.getString("telephone_number"));
            employer.setOther(rs.getString("other"));
            employer.setDefault();
            arrayList.add(employer);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
}
