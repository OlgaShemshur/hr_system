function backspaceAll(id)  {
    var item = document.getElementById(id);
    item.value = "";
}

function reWrite(id)  {
    var item = document.getElementById(id);
    if (item.value.length == 0) item.value = "Поиск...";
}

function send(id) {
    var item = document.getElementById('searchID');
    if (item.value != "Поиск...") {
        document.getElementById(id).submit();
    }
}

function sendOther(id) {
    document.getElementById(id).submit();
}

function hide(id) {
    document.getElementById(id).style.display = 'none';
}