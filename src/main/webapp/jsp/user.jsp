﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.ArrayList"%>
<html>
<head lang="ru">
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>${headers[0]}</title>
</head>
<body>
    <div class="tabloid"></div>
    <div class="under"></div>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div class="advertisement" id="advertisement">
            <img src="../IMG/cross.png" width="28" height="28" style="margin-bottom:45px; margin-left:1171px; position: absolute;" onclick="hide('advertisement');"/>
            <div>
                <div><a href='<c:out value="${advertisement[0]}"/>'><c:out value="${advertisement[1]}"/></a></div>
                <div><a href='<c:out value="${advertisement[2]}"/>'><c:out value="${advertisement[3]}"/></a></div>
                <div><a href='<c:out value="${advertisement[3]}"/>'><c:out value="${advertisement[5]}"/></a></div>
            </div>
        </div>
        <div id="mainLayer">
        <c:if test="${role=='user'}">
            <div class="rightBlock" style="height:360px;">
                <ul>
                    <li><c:out value="${user.surname}"/> <c:out value="${user.name}"/></li>
                    <li>${content[0]}<c:out value="${user.login}"/></li>
                    <li>${content[1]}<br><c:out value="${user.dateOfRegistration}"/></li>
                </ul>
            </div>
            <form action="Server" method="get" id="profile">
                <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 20px;" onclick="sendOther('profile');">
                    <img src="IMG/settings.png" alt="${content[2]}" width="50px" height="50px">
                    <span  class="text" style="bottom: 20px;">${content[2]}</span>
                    <input type="hidden" name="page" value="userProfile"/>
                </div>
            </form>
            <form action="Server" method="get" id="interviews">
                <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 100px;" onclick="sendOther('interviews');">
                    <img src="IMG/interview.png" alt="${content[3]}" width="50px" height="50px">
                    <span  class="text" style="bottom: 20px;">${content[3]}</span>
                    <input type="hidden" name="page" value="allInterviews"/>
                </div>
            </form>
            <form action="Server" method="get" id="interviewResults">
                <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 195px;" onclick="sendOther('interviewResults');">
                    <img src="IMG/pencil.png" alt="${content[4]}" width="50px" height="50px">
                    <span  class="text" style="bottom: 20px;">${content[4]}</span>
                    <input type="hidden" name="page" value="allInterviewResults"/>
                </div>
            </form>
            <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
            <c:set var="employer" value="${ob.getEmployerByID(item)}"/>
            <div class="dataFlash">
                <div>${content[5]}<c:out value="${ob.getCountOfInterviewsByIDofEmployee(user.id)}"/></div>
                <div>${content[6]}<c:out value="${ob.getCountOfInterviewResultsByIDofEmployee(user.id)}"/></div>
            </div>
        </c:if>
        <c:if test="${empty role}">
            <div class="text" style="width: 100%; height:auto; overflow:hidden;">${headers[20]}</div>
        </c:if>
        </div>
        <div class="menuLineBrick">
        <c:if test="${empty role}">
            <form action="Server" method="get" id="registration">
                <input type="hidden" name="page" value="registration"/>
                <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
            </form>
            <form action="Server" method="get" id="authorization">
                <input type="hidden" name="page" value="authorization"/>
                <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
            </form>
        </c:if>
        <c:if test="${role == 'user'}">
            <form action="Server" method="get" id="companies">
                <input value="${headers[18]}" type="submit">
                <input type="hidden" name="page" value="allCompanies"/>
            </form>
            <form action="Server" method="get" id="resume">
                <input value="${headers[19]}" type="submit">
                <input type="hidden" name="page" value="resume"/>
            </form>
        </c:if>
            <form action="Server" method="post" id="languageForm">
                <input type="hidden" name="action" value="language"/>
                    <span>${headers[1]}</span>
                    <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
                    <select onChange="sendOther('languageForm');" name="type">
                        <option value="${headers[13]}">${headers[4]}</option>
                        <option value="${headers[14]}">${headers[5]}</option>
                    </select>
            </form>
            <c:if test="${role == 'user'}">
            <form action="Server" method="post" id="endSessionForm">
                <input type="hidden" name="action" value="endSession"/>
                <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
            </form>
            </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
        </div>
    </div>
    <div class="header">${headers[11]}</div>
    <div class="footer">${headers[12]}</div>
</body>
    <script src="../js/search.js"></script>
</html>