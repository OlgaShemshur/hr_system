﻿    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@page contentType="text/html;charset=UTF-8" language="java" %>
        <%@page import="java.util.ArrayList"%>
        <html>
            <head>
                <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
                <link href="../css/style.css" rel="stylesheet">
                <link href="../css/style2.css" rel="stylesheet">
                <link href="../css/style3.css" rel="stylesheet">
                <meta charset="UTF-8">
                <title>${headers[0]}</title>
            </head>
        <body>
            <div class="tabloid"></div>
            <div class="under"></div>
            <div class="menuLine"></div>
            <div class="searchMenu" id="menuBrick"></div>
            <div class="content">
            <div id="mainLayer">
                <c:if test="${role=='administrator'}">
                    <div class="rightBlock" style="height:360px;">
                        <ul>
                           <li>${content[0]}<c:out value="${user.login}"/></li>
                            <li>${content[1]}<br><c:out value="${user.dateOfRegistration}"/></li>
                        </ul>
                    </div>
                    <form action="Server" method="post" id="allUsers">
                        <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 20px;" onclick="sendOther('allUsers');">
                            <input type="hidden" name="action" value="moreInfo"/>
                            <input type="hidden" name="type" value="allUsers"/>
                            <span  class="text" style="position: relative;">${content[2]}</span>
                        </div>
                    </form>
                <form action="Server" method="post" id="companies">
                    <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 100px;" onclick="sendOther('companies');">
                        <span  class="text" style="position: relative;">${content[3]}</span>
                        <input type="hidden" name="action" value="moreInfo"/>
                        <input type="hidden" name="type" value="allCompanies"/>
                    </div>
                </form>
            </c:if>
            <c:if test="${empty role}">
                <div class="text" style="width: 100%; height:auto; overflow:hidden;">${headers[20]}</div>
            </c:if>
        <c:if test="${not empty failed}">
        <c:if test="${failed == 'false'}">
        <div class="text" style="width: 100%; height:auto; overflow:hidden; position: absolute; margin-top: 250px;">${content[10]}</div>
        </c:if>
        <c:if test="${failed == 'true'}">
        <div class="text" style="width: 100%; height:auto; overflow:hidden; position: absolute; margin-top: 250px;">${content[9]}</div>
        </c:if>
        </c:if>
        </div>
        <c:if test="${role=='administrator'}">
            <form class="table" action="Server" method="post" id="reg">
                <img src="../IMG/cross.png" width="28" height="28" style="margin-left:415px; position: absolute;" onclick="hide('reg');"/>
                    <div></div>
                    <div></div>
                    <div class="tr">
                        <label>
                            <span>${content[6]}</span>
                            <input type="text" name="login" requaried size="25" class="in" id="login" oninput="checkValidLogin('login')" onfocusout="resultCheck('login')">
                        </label>
                    </div>
                    <div class="tr">
                        <label>
                            <span>${content[7]}</span>
                            <input type="text" name="password" requaried size="25" class="in" id="password" oninput="checkValidLogin('password')" onfocusout="resultCheck('password')">
                        </label>
                    </div>
                    <div class="tr">
                        <input type="hidden" name="action" value="registration"/>
                        <input type="hidden" name="aim" value="administrator"/>
                        <input type="button" value="${content[8]}" class="button" onclick="sendOther('reg');">
                    </div>
                 </form>
            </c:if>
        <div class="menuLineBrick">
            <c:if test="${empty role}">
                <form action="Server" method="get" id="registration">
                    <input type="hidden" name="page" value="registration"/>
                    <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
                </form>
                <form action="Server" method="get" id="authorization">
                    <input type="hidden" name="page" value="authorization"/>
                    <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
                </form>
            </c:if>
            <c:if test="${role == 'administrator'}">
                <button type="submit" style="color:white; background-color:darkred" onclick="show('reg');">${content[5]}</button>
            </c:if>
            <form action="Server" method="post" id="languageForm">
                <input type="hidden" name="action" value="language"/>
                <span>${headers[1]}</span>
                <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
                <select onChange="sendOther('languageForm');" name="type">
                    <option value="${headers[13]}">${headers[4]}</option>
                    <option value="${headers[14]}">${headers[5]}</option>
                </select>
            </form>
            <c:if test="${role == 'administrator'}">
                <form action="Server" method="post" id="endSessionForm">
                    <input type="hidden" name="action" value="endSession"/>
                    <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
                </form>
            </c:if>
            </div>
            <div class="searchMenu" id="searchMenuSkewBrick"></div>
            <div class="searchMenu" id="searchMenuBrick">
                <div id="pagename">
                    <form action="Server" method="post" id="searchForm">
                        <select size="1" name="type">
                            <option value="1">${headers[6]}</option>
                            <option value="2">${headers[7]}</option>
                        </select>
                        <input type="hidden" name="action" value="searchItems"/>
                        <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                        <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
                    </form>
                </div>
            </div>
        </div>
        <div class="header">${headers[11]}</div>
        <div class="footer">${headers[12]}</div>
        </body>
        <script src="../js/validity.js"></script>
        <script src="../js/items.js"></script>
        <script src="../js/search.js"></script>
</html>