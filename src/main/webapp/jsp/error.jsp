﻿<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>${content[0]}</title>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style1.css" rel="stylesheet">
</head>
<body>
    <div class="tabloid"></div>
    <div class="under"></div>
    <div class="content">
        <div id="mainLayer">
            <div class="text" style="width: 100%; height:auto; overflow:hidden;">${content[1]}</div>
        </div>
    </div>
    <div class="header">
    <form action="Server" method="get" id="startPage">
    <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
    </form>
    ${content[5]}</div>
    <div class="footer">${headers[12]}</div>
</body>
    <script src="../js/search.js"></script>
</html>