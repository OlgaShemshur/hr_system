﻿<%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<html>
<head>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <title><c:out value="${headers[0]}"/></title>
</head>
<body>
    <div class="tabloid"></div>
    <div class="under"></div>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
    <div class="advertisement" id="advertisement">
        <img src="../IMG/cross.png" width="28" height="28" style="margin-bottom:45px; margin-left:1171px; position: absolute;" onclick="hide('advertisement');"/>
        <div>
            <div><a href='<c:out value="${advertisement[0]}"/>'><c:out value="${advertisement[1]}"/></a></div>
            <div><a href='<c:out value="${advertisement[2]}"/>'><c:out value="${advertisement[3]}"/></a></div>
            <div><a href='<c:out value="${advertisement[3]}"/>'><c:out value="${advertisement[5]}"/></a></div>
        </div>
    </div>
        <div id="mainLayer">
            <div class="text" id="contextBlock"><c:out value="${content[0]}"/>
                <div><c:out value="${content[1]}"/><c:out value="${quantityVacancy}"/></div>
                <div><c:out value="${content[2]}"/><c:out value="${quantityCompany}"/></div>
                <div><c:out value="${content[3]}"/><c:out value="${quantityEmployee}"/></div>
            </div>
            <div>
                <div class="block1 line">
                    <div class="block2"></div>
                    <form action="Server" method="get" id="registration">
                        <input type="hidden" name="page" value="registration"/>
                        <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
                    </form>
                    <form action="Server" method="get" id="authorization">
                        <input type="hidden" name="page" value="authorization"/>
                        <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
                    </form>
                </div>
                <div class="line">
                    <div class="flex-container">
                        <div class="flex-item"></div>
                        <div class="flex-item"></div>
                        <div class="flex-item"></div>
                    </div>
                </div>
            </div>
            <div id="note"><c:out value="${content[4]}"/></div>
            <div id="table" class="line" style="margin-top:80px">
                <div class="td">&nbsp</div>
                <div class="td">&nbsp</div>
                <div class="td">&nbsp</div>
            </div>
            <div class="line text" style="margin-left: 770px; margin-top:80px"><c:out value="${content[5]}"/></div>
        </div>
    <form action="Server" method="post" id="languageForm">
    <input type="hidden" name="action" value="language"/>
        <div class="menuLineBrick">
            <span>${headers[1]}</span>
            <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
            <select onChange="sendOther('languageForm');" name="type">
                <option value="${headers[13]}">${headers[4]}</option>
                <option value="${headers[14]}">${headers[5]}</option>
            </select>
        </div>
    </form>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
        </div>
    </div>
    <div class="header">${headers[11]}</div>
    <div class="footer">${headers[12]}</div>
</body>
    <script src="../js/search.js"></script>
</html>