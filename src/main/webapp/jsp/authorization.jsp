﻿<%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>${content[0]}</title>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style1.css" rel="stylesheet">
</head>
<body>
    <div class="tabloid"></div>
    <div class="under"></div>
    <div class="content">
        <div id="mainLayer">
            <div id="mess">
                <span id="inner_text_message"></span>
            </div>
            <c:if test="${not empty failed}">
                <c:if test="${failed == 'wrongData'}">
                    <div class="text" style="width: 100%; height:auto; overflow:hidden;">${content[8]}<br>${content[9]}
                        <ul>
                            <li>${content[10]}</li>
                            <li>${content[11]}</li>
                        </ul>
                    </div>
                </c:if>
                <c:if test="${failed == 'blockedAccount'}">
                    <div class="text" style="width: 100%; height:auto; overflow:hidden;">${content[7]}</div>
                </c:if>
            </c:if>
            <form class="table" action="Server" method="post" id="authorization">
                <div>${content[1]}</div>
                <div>${content[2]}</div>
                <div class="tr">
                    <label>
                        <span>${content[3]}</span>
                        <input type="text" name="login" requaried size="25" class="in" id="Name" oninput="checkValidLogin('Name')" onfocusout="resultCheck('Name')">
                    </label>
                </div>
                <div class="tr">
                    <label>
                        <span>${content[4]}</span>
                        <input type="password" name="password" requaried size="25" class="in" id="Password" onfocusout="checkValidPassword('Password')">
                    </label>
                </div>
                <div class="tr">
                    <input type="button" value="${content[5]}" class="button" onclick="checkAllForAutorisation()">
                    <input type="hidden" name="action" value="authorization"/>
                </div>
            </form>
        </div>
    </div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
    ${content[5]}</div>
    <div class="footer">${headers[12]}</div></div>
</body>
    <script src="../js/validity.js"></script>
    <script src="../js/search.js"></script>
</html>