﻿    <%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ page import="java.util.ArrayList"%>
        <html>
        <head>
        <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/style2.css" rel="stylesheet">
        <link href="../css/style3.css" rel="stylesheet">
        <title><c:out value="${headers[0]}"/></title>
        </head>
        <body>
        <div class="menuLine"></div>
        <div class="searchMenu" id="menuBrick"></div>
        <div class="content">
        <div id="mainLayer">
        <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
        <c:if test="${role == 'employer'}">
            <c:set var="user" value="${ob.getEmployerByID(userID)}"/>
            <c:set var="company" value="${ob.getCompanyById(user.idcompany)}"/>
            <c:set var="creator" value="${ob.getUserByID(company.iduser)}"/>
            <c:set var="vacancies" value="${ob.getCountOfVacanciesByIDofCompany(company.id)}"/>
            <div style="width:200px; height:100px; position: absolute; margin-left: 900px; margin-top: 28px;">
                <div style="position: absolute; z-index:10; color:white;">
                    <img src="../IMG/employer.png" width="100" height="100">
                    <span style="position: absolute; margin-top: -82px; margin-left: 100px;">${content[11]}</span>
                    <span style="position: absolute; margin-top: -55px; margin-left: 118px;">${vacancies}</span>
                </div>
                <div class="block2"></div>
            </div>
            <div style="width:200px; height:100px; position: absolute; margin-left: 650px; margin-top: 28px;">
                <div style="position: absolute; z-index:10; color:white; padding-left: 20px; padding-top: 5px;"></div>
            </div>
        </c:if>
        <div style="margin-top: 40px; margin-left: 20px;">
        <div class="titleName">${content[0]}<c:out value="${company.name}"/></div>
        <div>${content[1]}<c:out value="${company.address}"/></div>
        <div>${content[2]}<c:out value="${creator.surname}"/>  <c:out value="${creator.name}"/></div>
        <button id="more" onclick="showAll(); change('${content[10]}', '${content[3]}');">${content[3]}</button>
        </div>
        <div class="result hide" id="result">
        <c:set var="allData" value="${ob.getVacanciesByIDofCompany(company.id)}"/>
        <c:forEach var="item" items="${allData}" varStatus="status">
            <div>
            <div class="titleName"><c:out value="${item.position}"/></div>
            <div><b>${content[4]}
            <c:choose>
                <c:when test="${item.salary != 0.}">
                    <c:out value="${item.salary}"/> <c:out value="${item.salaryCurrency}"/>
                </c:when>
                <c:otherwise>
                    ${content[5]}
                </c:otherwise>
            </c:choose>
            </b></div>
            <div><c:out value="${item.requirements}"/></div>
            <c:set var="employer" value="${ob.getEmployerByIdOfVacancy(item)}"/>
            <c:set var="employer_user" value="${ob.getUserByID(employer.idemployee)}"/>
            <div>${content[6]}<c:out value="${employer_user.name}"/> <c:out value="${employer_user.surname}"/></div>
            <div>${content[7]}<c:out value="${item.timeAdded}"/></div>
            <form action="Server" method="post">
            <input type="hidden" name="action" value="moreInfo"/>
            <input type="hidden" name="itemID" value="${item.id}"/>
            <input type="hidden" name="type" value="vacancy"/>
            <button onclick="sendOther('form${status.index}');">${content[8]}</button>
            </form>
            </div>
        </c:forEach>
        <c:if test="${allData.size() == 0}">
            <div style="color: #650053;">
            <p style="padding-top:.33em">
            <b>${content[9]}</b>
            </div>
        </c:if>
        </div>
        </div>
        <c:if test="${role == 'employer'}">
        <form class="table" action="Server" method="post" id="reg">
            <img src="../IMG/cross.png" width="28" height="28" style="margin-left:415px; position: absolute;" onclick="hide('reg');"/>
                <div></div>
                <div>${content[12]}</div>
                <div class="tr">
                    <label>
                        <span>${content[13]}</span>
                        <input type="text" name="position" requaried size="25" class="in" id="position" oninput="checkValidLogin('position')" onfocusout="resultCheck('position')">
                    </label>
                </div>
                <div class="tr">
                    <label>
                        <span>${content[14]}</span>
                        <input type="text" name="salary" requaried size="25" class="in" id="salary" onfocusout="checkValidLogin('salary')">
                    </label>
                </div>
                <div class="tr">
                <label>
                    <span>${content[15]}</span>
                    <input type="text" name="salary_currency" requaried size="3" class="in" id="salary_currency" onfocusout="checkValidLogin('salary_currency')">
                </label>
                <label>
                    <span>${content[16]}</span>
                    <textarea rows ="19" cols="54" name="other"></textarea>
                </label>
            </div>
                <div class="tr">
                    <input type="hidden" name="action" value="registration"/>
                    <input type="hidden" name="aim" value="vacancy"/>
                    <input type="button" name="registerCompany" value="${content[17]}" class="button" onclick="sendOther('reg')">
                </div>
            </form>
        </c:if>
        <div class="menuLineBrick">
        <c:if test="${empty role}">
            <form action="Server" method="get" id="registration">
                <input type="hidden" name="page" value="registration"/>
                <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
            </form>
            <form action="Server" method="get" id="authorization">
                <input type="hidden" name="page" value="authorization"/>
                <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
            </form>
        </c:if>
        <c:if test="${role == 'employer'}">
            <button type="submit" style="color:white; background-color:darkred" onclick="show('reg');">${content[18]}</button>
            <form action="Server" method="get" id="resume">
                <input value="${headers[21]}" type="submit">
                <input type="hidden" name="page" value="resume"/>
            </form>
        </c:if>
        <form action="Server" method="post" id="languageForm">
            <input type="hidden" name="action" value="language"/>
            <span>${headers[1]}</span>
            <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
            <select onChange="sendOther('languageForm');" name="type">
                <option value="${headers[13]}">${headers[4]}</option>
                <option value="${headers[14]}">${headers[5]}</option>
            </select>
        </form>
        <c:if test="${not empty role}">
            <form action="Server" method="post" id="endSessionForm">
            <input type="hidden" name="action" value="endSession"/>
            <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
            </form>
        </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
        <form action="Server" method="post" id="searchForm">
        <select size="1" name="type">
        <option value="1">${headers[6]}</option>
        <option value="2">${headers[7]}</option>
        </select>
        <input type="hidden" name="action" value="searchItems"/>
        <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
        <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
        </form>
        </div>
        </div>
        </div>
        <div style="position:absolute; margin-top: 0;" id="start"></div>
        <div class="whiteShadow"></div>
        <div class="header">
        <form action="Server" method="get" id="startPage">
        <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
        </div>
        <div class="footer">${headers[12]}</div>
        <a href="#start"><div class="up">${headers[16]}</div></a>
        </body>
        <script src="../js/search.js"></script>
        <script src="../js/items.js"></script>
        <script src="../js/validity.js"></script>
        </html>