﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.ArrayList"%>
<html>
<head lang="ru">
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <link href="../css/style3.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title><c:out value="${headers[0]}"/></title>
</head>
<body>
   <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div class="advertisement" id="advertisement">
            <img src="../IMG/cross.png" width="28" height="28" style="margin-bottom:45px; margin-left:1171px; position: absolute;" onclick="hide('advertisement');"/>
            <div>
                <div><a href='<c:out value="${advertisement[0]}"/>'><c:out value="${advertisement[1]}"/></a></div>
                <div><a href='<c:out value="${advertisement[2]}"/>'><c:out value="${advertisement[3]}"/></a></div>
                <div><a href='<c:out value="${advertisement[3]}"/>'><c:out value="${advertisement[5]}"/></a></div>
            </div>
        </div>
        <div id="mainLayer">
            <div class="result">
                <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
                <c:set var="item" value="${ob.getVacancyByID(id)}"/>
                <div>
                    <div class="titleName">${content[0]}<c:out value="${item.position}"/></div>
                    <div><b>${content[1]}
                    <c:choose>
                        <c:when test="${item.salary != 0.}">
                            <c:out value="${item.salary}"/> <c:out value="${item.salaryCurrency}"/>
                        </c:when>
                        <c:otherwise>${content[2]}</c:otherwise>
                    </c:choose>
                    </b></div>
                    <div><c:out value="${item.requirements}"/></div>
                    <c:set var="employer" value="${ob.getEmployerByIdOfVacancy(item)}"/>
                    <c:set var="user" value="${ob.getUserByID(employer.idemployee)}"/>
                    <div>${content[3]}<c:out value="${employer.email}"/> <c:out value="${employer.telephoneNumber}"/></div>
                    <div><c:out value="${user.surname}"/> <c:out value="${user.name}"/></div>
                    <div><c:out value="${employer.other}"/></div>
                    <div class="link">
                    <c:set var="company" value="${ob.getCompanyByID(employer)}"/>
                        <form action="Server" method="post" id="companyForm">
                            <div onclick="sendOther('companyForm');">${content[4]}<c:out value="${company.name}"/></div>
                            <input type="hidden" name="action" value="moreInfo"/>
                            <input type="hidden" name="type" value="company"/>
                            <input type="hidden" name="itemID" value="${company.id}"/>
                        </form>
                    </div>
                    <c:if test="${role=='user'}">
                        <form action="Server" method="post" id="reg">
                            <button onclick="sendOther('reg');">${content[7]}</button>
                            <input type="hidden" name="action" value="registration"/>
                            <input type="hidden" name="aim" value="interview"/>
                            <input type="hidden" name="itemID" value="${item.id}"/>
                        </form>
                    </c:if>
                    <c:if test="${empty role}">
                        <div style="color:#730f35; background-color: #d6d6d6;">${content[5]}</div>
                    </c:if>
                    <c:if test="${role=='employer'}">
                        <button class="line" id="more" onclick="showAll(); change('${content[10]}', '${content[9]}');">${content[9]}</button>
                        <form action="Server" method="post" class="line">
                            <input type="hidden" name="action" value="delete"/>
                            <input type="hidden" name="type" value="vacancy"/>
                            <input type="hidden" name="itemID" value="${item.id}"/>
                            <button>${content[8]}</button>
                        </form>
                    </c:if>
                    <div>${content[6]}${item.timeAdded}</div>
                </div>
            </div>
            <c:if test="${role=='employer'}">
                <div class="result hide" id="result">
                    <c:set var="allData" value="${ob.getInterviewsByIDofVacancy(item.id)}"/>
                    <c:forEach var="interview" items="${allData}" varStatus="status">
                        <div>
                            <c:set var="applicant" value="${ob.getUserByID(interview.idapplicant)}"/>
                            <c:set var="data" value="${ob.getEmployeeById(interview.idapplicant)}"/>
                            <div>${applicant.name} ${applicant.surname}</div>
                            <div>${content[13]}${data.dateOfBirthday}</div>
                            <div>${content[14]}
                                <c:if test="${data.sex == 'м'}">${content[19]}</c:if>
                                <c:if test="${data.sex == 'ж'}">${content[18]}</c:if>
                                <c:if test="${data.sex == 'н'}">${content[20]}</c:if>
                            </div>
                            <div>${content[15]}${data.email} ${data.telephoneNumber}</div>
                            <div>${content[16]}${data.education}</div>
                            <div>${content[17]}</div>
                            <div style="height:70px; overflow:auto;">${data.other}</div>
                            <div>
                                <form action="Server" method="post" class="line">
                                    <input type="hidden" name="action" value="update"/>
                                    <input type="hidden" name="type" value="interviewTime"/>
                                    <input type="hidden" name="itemID" value="${interview.id}"/>
                                    <div>${interview.timeOfInterview}</div>
                                    <div><input type="date" name="date"/><input type="time" name="time"/></div>
                                    <button onclick="sendOther('form${status.index}');">${content[11]}</button>
                                </form>
                                <form action="Server" method="post" class="line">
                                    <input type="hidden" name="action" value="delete"/>
                                    <input type="hidden" name="type" value="interview"/>
                                    <input type="hidden" name="itemID" value="${interview.id}"/>
                                     <button>${content[8]}</button>
                                </form>
                                <form class="table" action="Server" method="post" style="margin-top: -25%; position: fixed;">
                                    <img src="../IMG/cross.png" width="28" height="28" style="margin-left:415px; position: absolute;" onclick="hide('form${status.index*3+2}');"/>
                                    <div></div>
                                    <div>${applicant.name} ${applicant.surname}</div>
                                    <div class="tr">
                                        <label>
                                            <span>${content[22]}</span>
                                            <input type="text" name="mark" requaried size="4" class="in" id="mark">
                                        </label>
                                    </div>
                                    <div class="tr">
                                        <label>
                                            <span>${content[23]}</span>
                                            <textarea rows ="5" cols="54" name="recall"></textarea>
                                        </label>
                                    </div>
                                    <div class="tr">
                                        <input type="hidden" name="action" value="registration"/>
                                        <input type="hidden" name="aim" value="interviewResult"/>
                                        <input type="hidden" name="itemID" value="${interview.id}"/>
                                        <c:set var="i" value="${ob.getUserByID(interview.idapplicant)}"/>
                                        <button onclick="sendOther('form${status.index*3+2}');">${content[21]}</button>
                                    </div>
                                </form>
                                <c:if test="${interview.isActive == 'false'}" >
                                    <c:if test="${interview.isActive == 'false'}" >
                                        <button onclick="show('form${status.index*3+2}');">${content[21]}</button>
                                    </c:if>
                                </c:if>
                            </div>
                            <div>${content[6]}${interview.timeAdded}</div>
                        </div>
                    </c:forEach>
                    <c:if test="${allData.size() == 0}">
                        <div style="color: #650053;">
                            <p style="padding-top:.33em">
                            <b>${content[12]}</b>
                        </div>
                    </c:if>
                </div>
            </c:if>
        </div>
        <div class="menuLineBrick">
    <c:if test="${empty role}">
    <form action="Server" method="get" id="registration">
    <input type="hidden" name="page" value="registration"/>
    <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
    </form>
    <form action="Server" method="get" id="authorization">
    <input type="hidden" name="page" value="authorization"/>
    <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
    </form>
    </c:if>
    <c:if test="${role == 'user'}">
    <form action="Server" method="get" id="companies">
    <input value="${headers[18]}" type="submit">
    <input type="hidden" name="page" value="allCompanies"/>
    </form>
    <form action="Server" method="get" id="resume">
    <input value="${headers[19]}" type="submit">
    <input type="hidden" name="page" value="resume"/>
    </form>
    </c:if>
    <form action="Server" method="post" id="languageForm">
                <input type="hidden" name="action" value="language"/>
                <span>${headers[1]}</span>
                <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
                <select onChange="sendOther('languageForm');" name="type">
                    <option value="${headers[13]}">${headers[4]}</option>
                    <option value="${headers[14]}">${headers[5]}</option>
                </select>
            </form>
    <c:if test="${role == 'user' || role == 'employer'}">
    <form action="Server" method="post" id="endSessionForm">
    <input type="hidden" name="action" value="endSession"/>
    <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
    </form>
    </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
        </div>
    </div>
    <div style="position:absolute; margin-top: 0;" id="start"></div>
    <div class="whiteShadow"></div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
    </div>
    <div class="footer">${headers[12]}</div>
    <a href="#start"><div class="up">${headers[16]}</div></a>
</body>
    <script src="../js/search.js"></script>
    <script src="../js/items.js"></script>
    <script src="../js/validity.js"></script>
</html>